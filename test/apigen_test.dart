import 'package:redstone/redstone.dart' as app;
import 'package:sqlrest/sqlrest.dart';

main() {
  app.setupConsoleLog();
  app.start();
}