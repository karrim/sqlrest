-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2015-12-03 16:44:25.964


CREATE DOMAIN complexity int CHECK (VALUE > 0 AND VALUE < 6);
CREATE TYPE geographic_distribution AS ENUM ('Nord-ovest', 'Nord-est', 'Centro', 'Isole');
CREATE TYPE montano AS ENUM ('NM', 'T', 'P');
CREATE TYPE role AS ENUM ('Admin', 'User');



-- tables
-- Table: billing
CREATE TABLE billing (
    id serial  NOT NULL,
    company_name varchar(120)  NOT NULL,
    tax_code char(16)  NOT NULL,
    vat_number char(11)  NOT NULL,
    user_id int  NOT NULL,
    CONSTRAINT u_billing__tax_code UNIQUE (tax_code) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT billing_pk PRIMARY KEY (id)
);



-- Table: comune
CREATE TABLE comune (
    id int  NOT NULL,
    altitude_zone int  NOT NULL CHECK (altitude_zone > 0 AND altitude_zone <= 5),
    center_altitude int  NOT NULL,
    chief_town boolean  NOT NULL,
    coastal boolean  NOT NULL,
    mountain montano  NOT NULL,
    name varchar(50)  NOT NULL,
    name_german varchar(60)  NULL,
    surface int  NOT NULL,
    population_2001 int  NOT NULL,
    population_2011 int  NOT NULL,
    province_id int  NOT NULL,
    CONSTRAINT comune_pk PRIMARY KEY (id)
);



-- Table: dish
CREATE TABLE dish (
    id serial  NOT NULL,
    complexity complexity  NOT NULL,
    description varchar(1000)  NOT NULL,
    image varchar(4096)  NOT NULL,
    name varchar(120)  NOT NULL,
    time int  NOT NULL CHECK (time >= 0),
    CONSTRAINT u_dish__name UNIQUE (image) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT dish_pk PRIMARY KEY (id)
);



-- Table: province
CREATE TABLE province (
    id int  NOT NULL,
    name varchar(40)  NOT NULL,
    short_name char(2)  NOT NULL,
    region_id int  NOT NULL,
    CONSTRAINT u_province__name UNIQUE (name) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT u_province__short_name UNIQUE (short_name) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT province_pk PRIMARY KEY (id)
);



-- Table: region
CREATE TABLE region (
    id int  NOT NULL,
    geo_dist geographic_distribution  NULL,
    name varchar(40)  NOT NULL,
    CONSTRAINT region_pk PRIMARY KEY (id)
);



-- Table: "user"
CREATE TABLE "user" (
    id serial  NOT NULL,
    name varchar(60)  NOT NULL,
    password char(64)  NOT NULL,
    role role  NOT NULL,
    salt char(32)  NOT NULL,
    surname varchar(60)  NOT NULL,
    tel_n varchar(13)  NOT NULL,
    username varchar(20)  NOT NULL,
    CONSTRAINT u_user__username UNIQUE (username) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT user_pk PRIMARY KEY (id)
);






-- views
-- View: user_billing
CREATE VIEW user_billing AS
SELECT U.id, U.name, U.password, U.role, U.salt, U.surname, U.tel_n, U.username, B.company_name, B.tax_code, B.vat_number FROM "user" U JOIN billing B ON U.id = B.user_id;







-- foreign keys
-- Reference:  billing_user (table: billing)


ALTER TABLE billing ADD CONSTRAINT billing_user
    FOREIGN KEY (user_id)
    REFERENCES "user" (id)
    ON DELETE  SET NULL
    ON UPDATE  CASCADE
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference:  comune_province (table: comune)


ALTER TABLE comune ADD CONSTRAINT comune_province
    FOREIGN KEY (province_id)
    REFERENCES province (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference:  province_region (table: province)


ALTER TABLE province ADD CONSTRAINT province_region
    FOREIGN KEY (region_id)
    REFERENCES region (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;






-- End of file.
