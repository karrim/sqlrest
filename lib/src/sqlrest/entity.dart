library sqlrest.src.entity;

import 'package:sqlrest/sqlrest.dart';

const String _SQL_EXCEPTION_HANDLER = '''
void handleSqlException(Exception e) {
  throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
}''';

class Entity {
  final String sqlExceptionHandler;
  final List<Field> fields;
  final List<Key> keys;
  final String name;

  Entity(this.name, this.fields, this.keys,
      {this.sqlExceptionHandler: _SQL_EXCEPTION_HANDLER});

  operator ==(Entity e) => name == e.name && fields == e.fields;

  @override
  String toString() => '$name:($fields)';
}

class Field {
  final bool isNullable;

  final String name;
  final int size;
  final String type;

  Field(this.name, this.type, this.size, this.isNullable);

  operator ==(Field f) => isNullable == f.isNullable &&
      name == f.name &&
      size == f.size &&
      type == f.type;

  @override
  String toString() =>
      '$name $type${null == size ? '': '($size)'} ${isNullable ? 'NULL ' : 'NOT NULL'}';
}
