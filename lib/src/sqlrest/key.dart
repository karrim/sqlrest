library sqlrest.src.key;

import 'package:sqlrest/sqlrest.dart';

class ForeignKey extends Key {
  final Entity referencedEntity;

  final Field referencedField;
  ForeignKey(Field field, this.referencedEntity, this.referencedField)
      : super(KeyType.FOREIGN_KEY, <Field>[field]);
}

class Key {
  final List<Field> fields;

  final KeyType type;
  Key(this.type, this.fields);
}

class KeyType {
  static const KeyType FOREIGN_KEY = const KeyType._('FOREIGN_KEY'),
      PRIMARY_KEY = const KeyType._('PRIMARY_KEY'),
      UNIQUE_KEY = const KeyType._('UNIQUE_KEY');

  final String value;

  const KeyType._(this.value);
}

class PrimaryKey extends Key {
  PrimaryKey(List<Field> fields) : super(KeyType.PRIMARY_KEY, fields);
}

class UniqueKey extends Key {
  UniqueKey(List<Field> fields) : super(KeyType.UNIQUE_KEY, fields);
}
