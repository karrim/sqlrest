library sqlrest.src.type;

const Map<String, String> _sqlToDart = const {
  'boolean': 'bool',
  'char': 'String',
  'date': 'DateTime',
  'double': 'double',
  'float': 'float',
  'float4': 'float',
  'float8': 'float',
  'int': 'int',
  'int2': 'int',
  'int4': 'int',
  'int8': 'int',
  'numeric': 'String',
  'serial': 'int',
  'varchar': 'String',
  'text': 'String',
  'timestamp': 'DateTime',
  'timestamptz': 'DateTime'
};

SqlTypeMapper sqlTypeMapper({String onMismatch(String)}) {
  String sqlTypeToDart(String type) {
    if (_sqlToDart.containsKey(type)) {
      return _sqlToDart[type];
    }
    return null == onMismatch ? 'String' : onMismatch(type);
  }
  return sqlTypeToDart;
}

typedef String SqlTypeMapper(String type);
