library sqlrest.src.generate;

import 'package:path/path.dart';
import 'package:sqlrest/sqlrest.dart';

final Context context = new Context(style: Style.url);

class RedstoneTranslator {
  final Entity entity;
  final String restPath;
  final SqlTypeMapper typeMapper;

  RedstoneTranslator(this.entity, this.restPath, this.typeMapper);

  String get imports => '''
library sqlrest.src.generated.${entity.name};

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:redstone/redstone.dart' as app;
import 'package:redstone_mapper/mapper.dart';
import 'package:redstone_mapper/plugin.dart';
import 'package:redstone_mapper_pg/manager.dart';
import 'package:redstone_mapper_pg/service.dart';''';

  PrimaryKey get _primaryKey => entity.keys.firstWhere((k) => k is PrimaryKey);

  String asClass() {
    final className = _firstUpperCase(_camelCase(entity.name));
    var buf = new StringBuffer('''
class $className {
  $className();

  $className.build({''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('this.$name');
      if (field != entity.fields.last) {
        buf.write(', ');
      }
    }
    buf.write('''});

  $className.fromJson(Map<String, dynamic> json)
      : ''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('$name = json[\'${field.name}\']');
      if (field != entity.fields.last) {
        buf.write(', \n        ');
      }
    }
    buf.write(''';\n''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('\n');
      /*if (!field.isNullable) {
        buf.write('''
  @NotNull()
''');
      }*/
      buf.write('''
  @Field()
  ${typeMapper(field.type)} $name;
''');
    }
    buf.write('''

  Map<String, dynamic> toJson() => {''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('''

    '${field.name}': $name''');
      if (field != entity.fields.last) {
        buf.write(',');
      }
    }
    buf.write('''

  };

  String toString() => JSON.encode(toJson());
}''');
    return buf.toString();
  }

  String asResource() {
    final varName = _camelCase(entity.name);
    final className = _firstUpperCase(varName);
    final serviceName = '${varName}Service';
    return '''
@app.Group('${context.join(restPath, entity.name)}')
class ${className}Resource {

  final PostgreSqlService<$className> $serviceName = new PostgreSqlService<$className>();

  PostgreSql get postgres => app.request.attributes.dbConn;

  @app.Route('/', methods: const[app.POST])
  Future<$className> create(@Decode() $className $varName) async {
    $serviceName.execute('INSERT INTO ${entity.name} (${_fieldsNoPrimaryKey()}) VALUES (${_fieldsNoPrimaryKey(transform: (f) => '@${_camelCase(f.name)}')})', $varName);
    final row = await postgres.innerConn.query('SELECT ${_fieldsPrimaryKey()} FROM ${entity.name} WHERE ${_fieldsNoPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')}', {${_fieldsNoPrimaryKey(separator: ',', transform: (f) => "\n        '${f.name}': $varName.${_camelCase(f.name)}" )}}).single;
    ${_fieldsPrimaryKey(separator: ';\n      ', transform: (f) => '$varName.${_camelCase(f.name)} = row[${_primaryKey.fields.indexOf(f)}]')};
    return $varName;
  }

  @app.Route('/', methods: const [app.DELETE])
  Future delete(@Decode() $className $varName) async {
    return postgres.innerConn.execute('DELETE FROM ${entity.name} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')};', $varName);
  }

  @app.Route('/find', methods: const [app.POST])
  Future<$className> find(@Decode() $className $varName) async {
    return (await $serviceName.query('SELECT ${_fields(entity.fields, transform: (f) => '${f.name} AS "${_camelCase(f.name)}"')} FROM ${entity.name} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${_camelCase(f.name)}')};', $varName)).single;
  }

  @app.Route('/search', methods: const [app.POST])
  Future<List<$className>> search(@Decode() $className $varName) async {
    var fields = [], buf = new StringBuffer('SELECT ${_fields(entity.fields, transform: (f) => '${f.name} AS "${_camelCase(f.name)}"')} FROM ${entity.name} WHERE ');
${_fields(entity.fields, separator: '\n', transform: (f) => '''
    if(null != $varName.${_camelCase(f.name)}) {
      fields.add('${f.name}');
    }''')}
    if(fields.isEmpty) {
      throw new app.ErrorResponse(HttpStatus.BAD_REQUEST, 'No valid fields');
    }
    final reg = new RegExp(r'_\\w');
    for(var field in fields) {
      buf.write('\$field = @\${field.replaceAllMapped(reg, (m) => m[0].substring(1).toUpperCase())}');
      if(field != fields.last) {
        buf.write(' AND ');
      }
    }
    return await $serviceName.query(buf.toString(), $varName);
  }

  @app.Route('/', methods: const [app.PUT])
  Future<$className> update(@Decode() $className $varName) async {
    if(${_fieldsPrimaryKey(separator: ' || ', transform: (f) => 'null == $varName.${_camelCase(f.name)}')}) {
      throw new app.ErrorResponse(HttpStatus.BAD_REQUEST, 'Bad Request');
    }
    await $serviceName.execute('UPDATE ${entity.name} SET ${_fieldsNoPrimaryKey(transform: (f) => '${f.name} = @${_camelCase(f.name)}')} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${_camelCase(f.name)}')}', $varName);
    return $varName;
  }
}''';
  }

  @override
  String toString() => '''
$imports

${asClass()}

${asResource()}
''';

  String _camelCase(String value) {
    final split = value.split('_');
    if (split.isEmpty) {
      return value;
    }
    final buf = new StringBuffer(split.first);
    for (var s in split.skip(1)) {
      buf.write(_firstUpperCase(s));
    }
    return buf.toString();
  }

  String _fields(Iterable<Field> fields,
      {String separator: ', ', String transform(Field field)}) {
    final buf = new StringBuffer();
    for (var field in fields) {
      if (null == transform) {
        buf.write(field.name);
      } else {
        final t = transform(field);
        if (null == t) {
          continue;
        }
        buf.write(t);
      }
      if (field != fields.last) {
        buf.write(separator);
      }
    }
    return buf.toString();
  }

  String _fieldsNoPrimaryKey(
      {String separator: ', ', String transform(Field field)}) {
    final key = _primaryKey;
    return _fields(entity.fields.where((f) => !key.fields.contains(f)),
        separator: separator, transform: (f) {
          if (null == transform) {
            return f.name;
          }
          return transform(f);
        });
  }

  String _fieldsPrimaryKey(
      {String separator: ', ', String transform(Field field)}) {
    final key = _primaryKey;
    return _fields(entity.fields.where((f) => key.fields.contains(f)),
        separator: separator, transform: (f) {
          if (null == transform) {
            return f.name;
          }
          return transform(f);
        });
  }

  String _firstLowerCase(String value) =>
      '${value.substring(0, 1).toLowerCase()}${value.substring(1)}';

  String _firstUpperCase(String value) =>
      '${value.substring(0, 1).toUpperCase()}${value.substring(1)}';
}


class EntityTranslator {
  final Entity entity;
  final List<String> injectedTypes;

  final SqlTypeMapper typeMapper;

  EntityTranslator(this.entity, this.injectedTypes, this.typeMapper);

  String get imports => '''
library sqlrest.src.generated.${entity.name};

import 'dart:async';
import 'dart:io' hide HttpException;

import 'package:constrain/constrain.dart';
import 'package:http_exception/http_exception.dart';
import 'package:postgresql/pool.dart';
import 'package:postgresql/postgresql.dart';
import 'package:shelf_route/shelf_route.dart';''';

  PrimaryKey get _primaryKey => entity.keys.firstWhere((k) => k is PrimaryKey);

  String asClass() {
    final className = _firstUpperCase(_camelCase(entity.name));
    var buf = new StringBuffer('''
class $className {
  $className.build({''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('this.$name');
      if (field != entity.fields.last) {
        buf.write(', ');
      }
    }
    buf.write('''}) {
    validate();
  }

  $className.fromJson(Map<String, dynamic> json)
      : ''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('$name = json[\'${field.name}\']');
      if (field != entity.fields.last) {
        buf.write(', \n        ');
      }
    }
    buf.write(''' {
    validate();
   }\n''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('\n');
      if (!field.isNullable) {
        buf.write('''
  @NotNull()
''');
      }
      //TODO create validator classes
      /*if (null != field.size) {
        buf.write('''
  @Ensure((value) => value.length <= ${field.size})
''');
      }*/
      //TODO add getters and setters with validation
      buf.write('''
  ${typeMapper(field.type)} $name;
''');
    }
    buf.write('''

  Map<String, dynamic> toJson() => {''');
    for (var field in entity.fields) {
      final name = _camelCase(field.name);
      buf.write('''

    '${field.name}': $name''');
      if (field != entity.fields.last) {
        buf.write(',');
      }
    }
    buf.write('''

  };

  Set<ConstraintViolation> validate() {
    return _violations = _validator.validate(this);
  }

  bool get isValid => _violations.isEmpty;

  bool get isNotValid => _violations.isNotEmpty;

  Set<ConstraintViolation> get violations => _violations;

  Set<ConstraintViolation> _violations;
  static final Validator _validator = new Validator();
}''');
    return buf.toString();
  }

  String asResource({List<String> injectedTypes: const []}) {
    final varName = _camelCase(entity.name);
    final className = _firstUpperCase(varName);
    return '''
class ${className}Resource {

  Future<$className> create($className $varName${_injectedTypes(
      injectedTypes)}) async {
    if($varName.isNotValid) {
      final violations = $varName.violations;
    }
    var conn;
    try {
      conn = await pool.connect();
      await conn.execute('INSERT INTO ${entity.name} (${_fieldsNoPrimaryKey()}) VALUES (${_fieldsNoPrimaryKey(transform: (f) => '@${f.name}')})', {${_fieldsNoPrimaryKey(separator: ',', transform: (f) => "\n        '${f.name}': $varName.${_camelCase(f.name)}" )}});
      final row = await conn.query('SELECT ${_fieldsPrimaryKey()} FROM ${entity.name} WHERE ${_fieldsNoPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')}', {${_fieldsNoPrimaryKey(separator: ',', transform: (f) => "\n        '${f.name}': $varName.${_camelCase(f.name)}" )}}).single;
      ${_fieldsPrimaryKey(separator: ';\n      ', transform: (f) => '$varName.${_camelCase(f.name)} = row[${_primaryKey.fields.indexOf(f)}]')};
    } on PostgresqlException catch(e) {
      handleSqlException(e);
    } on Error catch(e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
    }
    return $varName;
  }

  Future delete(${_fieldsPrimaryKey(transform: (f) => '${typeMapper(f.type)} ${f.name}')}${_injectedTypes(injectedTypes)}) async {
    var conn;
    try {
      conn = await pool.connect();
      await conn.execute('DELETE FROM ${entity.name} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')};', {
        ${_fieldsPrimaryKey(separator: ',\n        ', transform: (f) => "'${f.name}': ${f.name}")}
      });
    } on PostgresqlException catch(e) {
      handleSqlException(e);
    } on Error catch(e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
    }
  }

  Future<$className> find(${_fieldsPrimaryKey(transform: (f) => '${typeMapper(f.type)} ${f.name}')}${_injectedTypes(injectedTypes)}) async {
    var conn, $varName;
    try {
      conn = await pool.connect();
      final row = await conn.query('SELECT ${_fields(entity.fields)} FROM ${entity.name} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')};', {
        ${_fieldsPrimaryKey(separator: ',\n        ', transform: (f) => "'${f.name}': ${f.name}")}
      }).single;
      $varName = new $className.build(${_fields(entity.fields, transform: (f) => '${_camelCase(f.name)}: row[${entity.fields.indexOf(f)}]')});
    } on PostgresqlException catch(e) {
      handleSqlException(e);
    } on StateError {
      throw new NotFoundException({'id': id}, 'Invalid id \$id.');
    } on Error catch(e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
    }
    return $varName;
  }

  Future<List<$className>> search({${_fieldsNoPrimaryKey(transform: (f) => '${typeMapper(f.type)} ${_camelCase(f.name)}')}${_injectedTypes(injectedTypes)}}) async {
    var conn, found = <$className>[];
    try {
      conn = await pool.connect();
      var params = <String, dynamic>{};
${_fieldsNoPrimaryKey(separator: '\n', transform: (f) => '''
      if(null != ${_camelCase(f.name)}) {
        params[\'${f.name}\'] = ${_camelCase(f.name)};
      }''')}
      final keys = params.keys;
      if(keys.isEmpty) {
        throw new HttpException(HttpStatus.BAD_REQUEST, 'Bad Request');
      }
      final buf = new StringBuffer('SELECT ${_fields(entity.fields)} FROM ${entity.name} WHERE ');
      for(var key in keys) {
        buf.write('\$key = @\$key');
        if(key != keys.last) {
          buf.write(' AND ');
        }
      }
      final rows = await conn.query(buf.toString(), params);
      await for(var row in rows) {
        found.add(new $className.build(${_fields(entity.fields, transform: (f) => '${_camelCase(f.name)}: row[${entity.fields.indexOf(f)}]')}));
      }
    } on PostgresqlException catch(e) {
      handleSqlException(e);
    } on Error catch(e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
    }
    return found;
  }

  Future<$className> update($className $varName${_injectedTypes(injectedTypes)}) async {
    var conn;
    try {
      if(${_fieldsPrimaryKey(separator: ' || ', transform: (f) => 'null == $varName.${_camelCase(f.name)}')}) {
        throw new HttpException(HttpStatus.BAD_REQUEST, 'Bad Request');
      }
      conn = await pool.connect();
      await conn.execute('UPDATE ${entity.name} SET ${_fieldsNoPrimaryKey(transform: (f) => '${f.name} = @${f.name}')} WHERE ${_fieldsPrimaryKey(separator: ' AND ', transform: (f) => '${f.name} = @${f.name}')}', $varName.toJson());
    } on PostgresqlException catch(e) {
      handleSqlException(e);
    } on Error catch(e) {
      throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        'Internal Server Error: \$e.');
    }
    return $varName;
  }
}''';
  }

  String asRouteable({Iterable<Field> searchBy}) {
    final className = _firstUpperCase(_camelCase(entity.name));
    if (null == searchBy) {
      final key = entity.keys.firstWhere((k) => k is PrimaryKey);
      if (null != key) {
        searchBy = entity.fields.where((f) => !key.fields.contains(f));
      }
    }
    var buf = new StringBuffer('''
class ${className}Routeable extends Routeable {
  void createRoutes(Router router) {
    router
      ..delete('/${entity.name}/{id}', resource.delete)
      ..get('/${entity.name}{?''');
    for (var field in searchBy) {
      buf.write('${field.name}');
      if (field != searchBy.last) {
        buf.write(',');
      }
    }
    buf.write('''}\', resource.search)
      ..get('/${entity.name}/{id}', resource.find)
      ..post('/${entity.name}', resource.create)
      ..put('/${entity.name}', resource.update);
  }

  final ${className}Resource resource = new ${className}Resource();
}''');
    return buf.toString();
  }

  @override
  String toString() => '''
$imports

${entity.sqlExceptionHandler}

${asClass()}

${asResource(injectedTypes: injectedTypes)}

${asRouteable()}
''';

  String _camelCase(String value) {
    final split = value.split('_');
    if (split.isEmpty) {
      return value;
    }
    final buf = new StringBuffer(split.first);
    for (var s in split.skip(1)) {
      buf.write(_firstUpperCase(s));
    }
    return buf.toString();
  }

  String _fields(Iterable<Field> fields,
      {String separator: ', ', String transform(Field field)}) {
    final buf = new StringBuffer();
    for (var field in fields) {
      if (null == transform) {
        buf.write(field.name);
      } else {
        final t = transform(field);
        if (null == t) {
          continue;
        }
        buf.write(t);
      }
      if (field != fields.last) {
        buf.write(separator);
      }
    }
    return buf.toString();
  }

  String _fieldsNoPrimaryKey(
      {String separator: ', ', String transform(Field field)}) {
    final key = _primaryKey;
    return _fields(entity.fields.where((f) => !key.fields.contains(f)),
        separator: separator, transform: (f) {
      if (null == transform) {
        return f.name;
      }
      return transform(f);
    });
  }

  String _fieldsPrimaryKey(
      {String separator: ', ', String transform(Field field)}) {
    final key = _primaryKey;
    return _fields(entity.fields.where((f) => key.fields.contains(f)),
        separator: separator, transform: (f) {
      if (null == transform) {
        return f.name;
      }
      return transform(f);
    });
  }

  String _firstLowerCase(String value) =>
      '${value.substring(0, 1).toLowerCase()}${value.substring(1)}';

  String _firstUpperCase(String value) =>
      '${value.substring(0, 1).toUpperCase()}${value.substring(1)}';

  String _injectedTypes(List<String> types) {
    final buf = new StringBuffer();
    for (var type in types) {
      buf.write(', $type ${_firstLowerCase(type)}');
    }
    return buf.toString();
  }
}
