library sqlrest.src.parse;

import 'package:sqlrest/sqlrest.dart';

final RegExp alterRegex =
    new RegExp(r'ALTER\s+TABLE\s+"?(\w+)"?\s+([^;]+)', multiLine: true),
    constraintRegex = new RegExp(
        r'CONSTRAINT\s+(\w+)\s+(FOREIGN KEY|PRIMARY KEY|UNIQUE)\s*[(](\w+(?:,w+)*)[)](?:\s*REFERENCES\s+"?(\w+)"?\s*[(](\w+(?:,\w+)?)[)])?',
        multiLine: false),
    fieldRegex = new RegExp(
        r'\s*(\w+)\s+(\w+)(?:[(](\d+)[)])?(\s+((?:NOT)? NULL))',
        multiLine: false),
    tableRegex =
    new RegExp(r'CREATE\s+TABLE\s+"?(\w+)"?\s+\(([^;]+)', multiLine: true);

void parseConstraint(
    Match constraint, Entity entity, Map<String, Entity> entities) {
  final fieldNames = constraint[3].split(',');
  final fs = entity.fields.where((f) => fieldNames.contains(f.name));
  switch (constraint[2]) {
    case 'FOREIGN KEY':
      final refEntity = entities[constraint[4]];
      final refField =
          refEntity.fields.firstWhere((f) => f.name == constraint[5]);
      entity.keys.add(new ForeignKey(fs.first, refEntity, refField));
      break;
    case 'PRIMARY KEY':
      entity.keys.add(new PrimaryKey(fs.toList(growable: false)));
      break;
    case 'UNIQUE':
      entity.keys.add(new UniqueKey(fs.toList(growable: false)));
      break;
  }
}

List<Entity> parseSql(String sql) {
  final entities = <String, Entity>{};
  for (var table in tableRegex.allMatches(sql)) {
    final fields = <Field>[];
    final keys = <Key>[];
    final tableName = table[1];
    final tableContent = table[2];
    final entity = new Entity(tableName, fields, keys);
    entities[tableName] = entity;
    for (var field in fieldRegex.allMatches(table[2])) {
      final name = field[1];
      final type = field[2];
      var size = field[3];
      if (null != size) {
        size = int.parse(size);
      }
      final nullable = !field[4].contains('NOT');
      fields.add(new Field(name, type, size, nullable));
    }
    for (var constraint in constraintRegex.allMatches(tableContent)) {
      parseConstraint(constraint, entity, entities);
    }
  }
  for (var alter in alterRegex.allMatches(sql)) {
    final tableName = alter[1];
    final alterContent = alter[2];
    for (var constraint in constraintRegex.allMatches(alterContent)) {
      parseConstraint(constraint, entities[tableName], entities);
    }
  }
  return entities.values.toList(growable: false);
}
